const fs = require('fs');
const fetch = require("node-fetch");
const ncp = require('ncp').ncp;

var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
  };
 
ncp.limit = 16;

const token = 'e783e92234e85d48ad87d9d73c2294';

fetch(
  'https://graphql.datocms.com/',
  {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': `Bearer ${token}`,
    },
    body: JSON.stringify({
      query: '{ allNews { title, image { url } } }'
    }),
  }
)
.then(res => res.json())
.then((res) => {
    let images = '';
    res.data.allNews.forEach(element => {
        images = images + '<li><img src="' + element.image.url + '"/></li>'
    });
    let contents = fs.readFileSync('index.template', 'utf8');
    contents = contents.replace('{{IMAGES_HERE}}', images);
    deleteFolderRecursive('dist');
    fs.mkdirSync('dist');
    fs.writeFile('dist/index.html', contents, (err) => { 
        if (err) throw err;
        console.log('Index created!');
        ncp('static', 'dist', function (err) {
            if (err) {
              return console.error(err);
            }
            console.log('done!');
        });
    });
})
.catch((error) => {
  console.log(error);
});